﻿using System;
using System.Text.RegularExpressions;

namespace PoligonPoint
{
    public static class Regular
    {
        /// <summary>
        /// С помощью регулярного выражения на лету убираются символы, не входящие в 1-99
        /// </summary>
        /// <param name="val">Входное выражение</param>
        /// <returns>Выходное выражение</returns>
        public static string CheckText(string val)
        {
            string pattern = (@"([\D])|(^[^0-9][^0-9]$)");
            val = Regex.Replace(val, pattern, "");
            if (val.Length > 0)
                if (Int32.Parse(val) > 99 | val.Length > 2)
                    val = "0";
            
            return val;
        }

    }
}
