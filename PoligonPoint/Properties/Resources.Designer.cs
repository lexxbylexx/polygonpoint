﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PoligonPoint.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PoligonPoint.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Еще раз?.
        /// </summary>
        public static string Again {
            get {
                return ResourceManager.GetString("Again", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Проверка....
        /// </summary>
        public static string Check {
            get {
                return ResourceManager.GetString("Check", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выход.
        /// </summary>
        public static string Exit {
            get {
                return ResourceManager.GetString("Exit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Задайте количество вершин полигона от 3 до 99:.
        /// </summary>
        public static string Label1 {
            get {
                return ResourceManager.GetString("Label1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите кликом мыши вершины на поле окна. Осталось задать:.
        /// </summary>
        public static string Label2 {
            get {
                return ResourceManager.GetString("Label2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите свободную точку на поле окна.
        /// </summary>
        public static string Label3 {
            get {
                return ResourceManager.GetString("Label3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Значения должны быть от 3 до 99.
        /// </summary>
        public static string LabelError {
            get {
                return ResourceManager.GetString("LabelError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap page {
            get {
                object obj = ResourceManager.GetObject("page", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Точка не попала в полигон.
        /// </summary>
        public static string ResultFalse {
            get {
                return ResourceManager.GetString("ResultFalse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Точка попала в полигон.
        /// </summary>
        public static string ResultTrue {
            get {
                return ResourceManager.GetString("ResultTrue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Попадание точки в полигон.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
    }
}
