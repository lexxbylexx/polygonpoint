﻿using System;
using System.Windows;

namespace PoligonPoint
{
    class CheckPoint
    {
        /// <summary>
        /// Метод возвращает верно, если точка попадает в область полигона, или неверно, если нет. Способ трассировки лучом
        /// </summary>
        /// <param name="p">Массив точек полигона</param>
        /// <param name="x">Координата Х</param>
        /// <param name="y">Координата У</param>
        /// <returns>Результат проверки попадания</returns>
       public static bool IsPointInsidePolygon(Point[] p, double x, double y)
        {
            int c = 0;
            for (int i = 0, j = p.Length - 1; i < p.Length; j = i++)
                if ((((p[i].Y <= y) && (y < p[j].Y)) || ((p[j].Y <= y) && (y < p[i].Y))) &&
                  (x > (p[j].X - p[i].X) * (y - p[i].Y) / (p[j].Y - p[i].Y) + p[i].X))
                    c = 1 - c;

            if (c == 1) // точка внутри
                return true;

            return false; // иначе - снаружи
        }
    }
}
