﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace PoligonPoint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int i, count;
        private Point point;
        private Point[] massivP;
        private bool startCreatePoint= false;
        //ссылаемся на метод для проверки попадания точки в полигон
        delegate bool Check ( Point[] p, double x, double y);

    public MainWindow()
        {
            InitializeComponent();
            textbox.GotFocus += text_GotFocus;
            buttonAgain.Visibility = Visibility.Hidden;

            
        }

        private void text_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox text = sender as TextBox;
            if (text != null) text.SelectAll();
        }

        private void gridPoly_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //включено ли создание точки
            if (startCreatePoint)
            {
                if (i <= count)
                //создается массив точек и на экран выводятся временные точки
                {
                    if(count > i)
                    {
                        Point p = Mouse.GetPosition(canvasPoly);
                        canvasPoly.Children.Add(ShapePolygon.Create(p));
                        massivP[i] = p;
                        if (i<count-1)
                        {
                            labelHelp.Content = Properties.Resources.Label2+ " " + (count - i-1);
                        }
                        else
                        {
                            labelHelp.Content = Properties.Resources.Label3;
                            canvasPoly.Children.Clear();
                            canvasPoly.Children.Add(ShapePolygon.Create(massivP));
                            canvasPoly.Children.Add(labelHelp);
                        }
                        
                    }
                   
                    if (count == i)
                    {
                        
                        point = Mouse.GetPosition(canvasPoly);
                        canvasPoly.Children.Add(ShapePolygon.Create(point));
                        buttonAgain.Visibility = Visibility.Visible;
                        canvasPoly.Children.Add(buttonAgain);
                        Check check = new Check(CheckPoint.IsPointInsidePolygon);
                        BitmapImage bi3 = new BitmapImage();
                        canvasPoly.Children.Add(image);
                        if (check(massivP, point.X, point.Y))
                        {
                            labelHelp.Content = Properties.Resources.ResultTrue;
      
                            bi3.BeginInit();
                            bi3.UriSource = new Uri("Resources/true.png", UriKind.Relative);
                            bi3.EndInit();
                            image.Stretch = Stretch.Fill;
                            image.Source = bi3;
                            Anima();
                        }
                        else
                        {
                            labelHelp.Content = Properties.Resources.ResultFalse;

                            bi3.BeginInit();
                            bi3.UriSource = new Uri("Resources/false.png", UriKind.Relative);
                            bi3.EndInit();
                            image.Stretch = Stretch.Fill;
                            image.Source = bi3;
                            Anima();
                        }
                    }                  
                }

                i++;
            }         
        }

        //фильтрация ввода в текст бокс
        private void textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textbox = sender as TextBox;          
            textbox.Text = Regular.CheckText(textbox.Text);
            textbox.CaretIndex = textbox.Text.Length;
        }

        //Начать ли рисование полигона?
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Проверка данных
            if ((textbox.Text == "") | (textbox.Text == "0") | (textbox.Text == "00") | (textbox.Text == "1") | (textbox.Text == "2"))
            {
                labelHelp.Foreground = new SolidColorBrush(Colors.Red);
                labelHelp.Content = Properties.Resources.LabelError;
                
            }
            //Включаем координаты. Прячем кнопку и окно ввода. Задаем в массив количество точек полигона.
            else
            {
                
                startCreatePoint = true;
                textbox.Visibility = Visibility.Hidden;
                buttonStart.Visibility = Visibility.Hidden;

                labelHelp.Foreground = new SolidColorBrush(Colors.Black);
                labelHelp.Content = Properties.Resources.Label2 + " " + (int.Parse(textbox.Text));

                massivP = new Point[count = int.Parse(textbox.Text)];
              
            }
        }

        private void buttonAgain_Click(object sender, RoutedEventArgs e)
        {
            startCreatePoint = false;
            i = 0;
            labelHelp.Content = Properties.Resources.Label1;
            buttonAgain.Visibility = Visibility.Hidden;

            canvasPoly.Children.Clear();
            textbox.Visibility = Visibility.Visible;
            textbox.Text = "0";
            buttonStart.Visibility = Visibility.Visible;
            canvasPoly.Children.Add(labelHelp);
            canvasPoly.Children.Add(textbox);
            canvasPoly.Children.Add(buttonStart);
        }

        private void Anima()
        {
           
            var animation = new ThicknessAnimation();
            animation.From = new Thickness(0);
            animation.To = new Thickness(200);
            animation.Duration = TimeSpan.FromSeconds(3);
            image.BeginAnimation(MarginProperty, animation);
        }
    }
}
