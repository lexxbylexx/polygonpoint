﻿
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PoligonPoint
{
    public static class ShapePolygon
    {
        /// <summary>
        /// Создаем полигон либо точку полиморфизмом
        /// </summary>
        /// <param name="p">На вход массив точек для полигона или точка для точки</param>
        /// <returns>Фигура полигона или фигура точки(на основе эллипса)</returns>
        public  static Polygon Create(Point[] p)
        {
            // Создать синюю и черную кисти
            SolidColorBrush yellowBrush = new SolidColorBrush();
            yellowBrush.Color = Colors.Yellow;
            SolidColorBrush blackBrush = new SolidColorBrush();
            blackBrush.Color = Colors.Black;

            // Создать Полигон
            Polygon yellowPolygon = new Polygon();
            yellowPolygon.Stroke = blackBrush;
            yellowPolygon.Fill = yellowBrush;
            yellowPolygon.StrokeThickness = p.Length;

            // Создать коллекцию вершин полигона
            PointCollection polygonPoints = new PointCollection();
            foreach (var item in p)
            {
                polygonPoints.Add(item);
            }          

            // Задать свойства полигона
            yellowPolygon.Points = polygonPoints;

            // Добавить полигон на страницу
            return yellowPolygon;
        }

        public static Ellipse Create(Point p)
        {
            int dotSize = 10;

            Ellipse currentDot = new Ellipse();
            currentDot.Stroke = new SolidColorBrush(Colors.Green);
            currentDot.StrokeThickness = 1;
            
            currentDot.Height = dotSize;
            currentDot.Width = dotSize;
            currentDot.Fill = new SolidColorBrush(Colors.Green);
            currentDot.Margin = new Thickness(p.X, p.Y, 1, 1);

            // Добавить полигон на страницу
            return currentDot;
        }

    }
}
